import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import "./assets/font/font.css"
import Vant from 'vant';
import 'vant/lib/index.css';
import   server from './api/index'
import VueCookies from 'vue-cookies'
// import { BaseURL } from '../public/config'
import axios from 'axios'
axios.defaults.timeout = 10000;
Vue.prototype.$axios = axios
Vue.config.productionTip = false
Vue.prototype.$server = server;
// Vue.prototype.$BaseURL = BaseURL;
Vue.use(VueCookies)
Vue.use(Vant);
Vue.use(ElementUI);
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
