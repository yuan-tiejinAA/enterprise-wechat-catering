import axios from "axios";
const BaseURL = window.urlconfig.VUE_BASE_URL
console.log(window.urlconfig);
const server = axios.create({
  baseURL: BaseURL,
  timeout: 5000,
  withCredentials: true, // 启用跨域请求
  headers: {
    'Content-Type': 'application/json', //请求头
    'Authorization': 'Bearer login'
  }
});

// 请求拦截器
server.interceptors.request.use((config) => {
  if (localStorage.getItem('token')) {
    config.headers['Xshop-Token'] = JSON.parse(localStorage.getItem('token')).token;
  }
  return config;
});

// 响应拦截器
server.interceptors.response.use((response) => {
  // 在这里对响应数据进行处理
  return response.data;
}, (error) => {
  // 在这里对响应错误进行处理
  return Promise.reject(error);
});

export default server;
