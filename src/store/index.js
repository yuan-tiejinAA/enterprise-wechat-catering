import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    PageIfg: false,
    title:"美团",
    TopData:{},
    date:'',
    maxDate:'',
    minDate:'',
    tupIfg:false,
    Storeshow:false,
    showIndex:0,
    showData:{},
    varietyIfg:false,
    EchartsData:{
      date:['10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:30','20:00','20:30','21:00','21:30','22:00','22:30','23:00','23:30'],
      data:[125,150,245,250,130,150,140,160,150,180,190,200,190,220,230,220,250,150,270,280,290,220,310,320,330,290,350,360]
    },
    notIfg:false,
    url:window.urlconfig.VUE_BASE_SEND_URL //路由//路由
  },
  getters: {
  },
  mutations: {
    SetEchartsData(e,data){
      console.log(data);
      e.EchartsData=data
    },
    SetNotIfg(e,v){
      e.notIfg=v
    },
    SetStoreshow(e,data){
      console.log(data);
      e.Storeshow=data
    },
    SetshowIndex(e,data){
      e.showIndex=data
    },
    SetshowData(e,data){
      e.showData=data
    },
    SetPageIfg(e,v) {
      e.PageIfg=v.flg
      e.title=v.title
      e.TopData=v.data
    },
    SetTopData(e,data){
      e.TopData=data
    },
    SetDate(e,data){
      e.date=data
    },
    SetmaxDate(e,data){
      e.maxDate=data
    },
    SetminDate(e,data){
      e.minDate=data
    },
    setTupIfg(e,data){
      e.tupIfg=data
    }

  },
  actions: {
  },
  modules: {
  }
})
