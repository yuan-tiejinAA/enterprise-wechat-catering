import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home/index.vue'
import jie from '@/views/HomeOne/index.vue'
import Echarer from '@/views/Echarer/index.vue'
Vue.use(VueRouter)

const routes = [
  {
    path: '/home',
    name: 'home',
    component: Home,
    meta: {
      title: "汤富贵经营分析"
    }
  },
  {
    path: '/jie',
    name: 'jie',
    component: jie,
    meta: {
      title: "汤富贵结班表"
    }
  },
  {
    path: '/Echarer',
    name: "Echarer",
    component: Echarer,
  },
  {
    path: "/duo",
    name: "duo",
    component: () => import('@/views/duo/index.vue'),
  },
  {
    path: '/da',
    name: "da",
    component: () => import('@/components/tup/index.vue'),
  },
  {
    path: "/chefSort",
    name: "/chefSort",
    component: () => import('@/views/chefSort/index.vue'),
    meta: {
      title: "详细生产数据分析详情"
    }
  },
  {
    path: "/vari",
    name: "variety",
    component: () => import('@/components/not/index.vue'),
  },
  {
    path: "/sale",
    name: "sale",
    component: () => import('@/views/sale/index.vue'),
    meta: {
      title: "详细销售分析"
    }
  },
  {
    path: "/chefHome",
    name: "chefHome",
    component: () => import('@/views/chefHome/index.vue'),
    meta: {
      title: "详细生产数据分析"
    }
  },
  {
    path: "/store",
    name: "store",
    component: () => import("../views/storeData/index.vue"),
  },
  {
    path: "/storeTwo",
    name: "storeTwo",
    component: () => import("../views/storeData/indexTwo.vue"),
  },
  {
    path: "/storeThree",
    name: "storeThree",
    component: () => import("@/views/storeData/indexThree.vue"),
  },
  {
    path: "/storeFour",
    name: "storeFour",
    component: () => import("@/views/storeData/indexFour.vue"),
  },
  {
    path: "/task",
    name: "task",
    component: () => import("@/views/Task/index.vue"),
  },
  // {
  //   path:"/StoreDataTow",
  //   name:"StoreData",
  //   component:()=>import('@/components/StoreDataOne/index.vue'),
  // },
  // {
  //   path:"/StoreDataThree",
  //   name:"StoreData",
  //   component:()=>import('@/components/StoreDataThree/index.vue'),
  // },
  // {
  //   path:"/StoreDataFrom",
  //   name:"StoreData",
  //   component:()=>import('@/components/StoreDataTow/index.vue'),
  // }

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})
router.beforeEach((to, from, next) => {
  // 设置标题
  if (to.path === '/chefHome' || to.path === '/home' || to.path === '/sale' || to.path === '/chefSort'||to.path==='/jie') {
    document.title = to.meta.title;
    next();
  }
  next()

})
export default router
