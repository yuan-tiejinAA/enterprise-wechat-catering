const pako = require('./pako.js');
export function unZip(base64Str) {
    if (base64Str == null || base64Str == '' || base64Str == undefined) {
        return null;
    }
    var arrayTemp = base64ToArray(base64Str);
    var returnStr = pako.inflate(arrayTemp, {
        to: 'string'
    });
    return returnStr;
}
function base64ToArray(base64Data) {
    let base = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
    // base64Data.
    let equalCount = base64Data.match(/=/g) || 0;
    base64Data = base64Data.replace(/=/g, '');
    let len = base64Data.length;
    let mod = len % 4;
    let sum = Math.floor(len / 4);
    let idx = 0;
    let moreLen = 0;
    if (equalCount && equalCount.length == 1) moreLen = 2;
    if (equalCount && equalCount.length == 2) moreLen = 1;
    let buf = new Uint8Array(sum * 3 + moreLen);
    for (let i = 0; i < sum * 4; i += 4) {
        let char0 = base64Data[i];
        let char1 = base64Data[i + 1];
        let char2 = base64Data[i + 2];
        let char3 = base64Data[i + 3];
        let charIdx0 = base.indexOf(char0);
        let charIdx1 = base.indexOf(char1);
        let charIdx2 = base.indexOf(char2);
        let charIdx3 = base.indexOf(char3);
        if (charIdx0 == -1 || charIdx1 == -1 || charIdx2 == -1 || charIdx3 == -1) {
            continue;
        }
        // byte1 = 
        // 后6 前2
        buf[idx++] = (charIdx0 << 2) | (charIdx1 >> 4 & 0x03);
        buf[idx++] = (charIdx1 << 4) | (charIdx2 >> 2 & 0x0f);
        buf[idx++] = (charIdx2 << 6) | (charIdx3 & 0x3f);
    }
    if (equalCount && equalCount.length > 0) {
        if (equalCount.length == 1) {
            let charIdx0 = base.indexOf(base64Data[base64Data.length - 3]);
            let charIdx1 = base.indexOf(base64Data[base64Data.length - 2]);
            let charIdx2 = base.indexOf(base64Data[base64Data.length - 1]);
            // 一个等号说明有三位码，18bit(实16bit) 可以合成2位 
            buf[buf.length - 2] = (charIdx0 << 2) | (charIdx1 >> 4);
            buf[buf.length - 1] = (charIdx1 << 4) | (charIdx2 >> 2);
        } else if (equalCount.length == 2) {
            // 一个等号说明有二位码，可以合成1位
            let charIdx0 = base.indexOf(base64Data[base64Data.length - 2]);
            let charIdx1 = base.indexOf(base64Data[base64Data.length - 1]);
            buf[buf.length - 1] = (charIdx0 << 2) | (charIdx1 >> 4);
        }
    }
    return buf;
}